﻿using System;
using System.Threading;

namespace Pong
{
    class MainPong
    {
        public static int goBall = 0;
        public static int n1 = 0;
        public static int n2 = 0;
        public static int helpX = 0;
        public static int helpY = 0;
        public static int level = 0;
        static void Main(string[] args)
        {
            Console.WindowHeight = 25;
            Console.WindowWidth = 81;
            Console.BufferHeight = 25;
            Console.BufferHeight = 81;
            WallLeftAndRight();
            WallUpAndDown();
            PongWrite();
            try
            {
                Start();
            }
            catch (FormatException)
            {
                Game();
            }

        }

        public static void Game()
        {
            int x = 20;
            int y = 20;
            n1 = 0;
            n2 = 0;
            Ball ball = new Ball(x, y, 1, 1);
            TennisPlayer first = new TennisPlayer(6, 10);
            TennisPlayer second = new TennisPlayer(74, 10);
            try
            {
                Click(ball, first, second);
            }    
            catch (FormatException)
            {
                Console.CursorVisible = false;
                level = 5;
                Game();
            }
            }

        public static void Click(Ball ball, TennisPlayer first, TennisPlayer second)
        {
            Console.Clear();
            WallUpAndDown();
            WallLeftAndRight();
            first.Draw();
            second.Draw();
                        while (true)
            {
                Counts(n1, n2);
                Motion(ball, first, second);
                if (Console.KeyAvailable == true)
                {
                    switch (Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.S:
                            if (first.y < 18)
                            {
                                Console.SetCursorPosition(first.x, first.y);
                                Console.WriteLine(' ');
                                first.y++;
                                first.Draw();
                            }
                            break;

                        case ConsoleKey.W:
                            if (first.y > 3)
                            {
                                Console.SetCursorPosition(first.x, first.y+3);
                                Console.WriteLine(' ');
                                first.y--;
                                first.Draw();
                            }
                            break;

                        case ConsoleKey.DownArrow:
                            if (second.y < 18)
                            {
                                Console.SetCursorPosition(second.x, second.y);
                                Console.WriteLine(' ');
                                second.y++;
                                second.Draw();
                            }
                            break;

                        case ConsoleKey.UpArrow:
                            if (second.y > 3)
                            {
                                Console.SetCursorPosition(second.x, second.y + 3);
                                Console.WriteLine(' ');
                                second.y--;
                                second.Draw();
                            }
                            break;

                        case ConsoleKey.Backspace:
                                helpX = ball.dx;
                                helpY = ball.dy;
                                ball.dx = 0;
                                ball.dy = 0;
                            break;

                        case ConsoleKey.Spacebar:
                            ball.dx = helpX;
                            ball.dy = helpY;
                            break;

                    }
                }

                if (n1 == 10)
                {
                    Console.SetCursorPosition(29, 16);
                    Console.WriteLine("Победил первый игрок!!!");
                    Start();
                }

                else if (n2 == 10)
                {
                    Console.SetCursorPosition(29, 16);
                    Console.WriteLine("Победил второй игрок!!!");
                    Start();
                }
                Thread.Sleep(100-(level*10));
            }
           

        }

        public static void Motion(Ball ball, TennisPlayer first, TennisPlayer second)
        {
            if (ball.y > 20)
            {
                ball.y = 21;
                ball.dy = -1;
            }
            if (ball.y < 4)
            {
                ball.y = 3;
                ball.dy = 1;
            }
            if (ball.x > 76)
            {
                ball.x = 77;
                ball.dx = -1;
            }
            if (ball.x < 4)
            {
                ball.x = 3;
                ball.dx = 1;
            }
            for (int i=0; i<4; i++)
            {
                
                if (((ball.dy<0) && ((ball.x == first.x + 1 && ball.y == first.y + i+1) || (ball.x == second.x - 1 && ball.y == second.y + i+1))) || ((ball.dy > 0) && ((ball.x == first.x + 1 && ball.y == first.y + i -1) || (ball.x == second.x - 1 && ball.y == second.y + i - 1))))
                {
                        ball.dx *= -1;
                }
                if (((ball.dy < 0) && ((ball.x == first.x + 1 && ball.y == first.y + 3 + 1) || (ball.x == second.x + -1 && ball.y == second.y + 3 + 1))) || ((ball.dy > 0) && ((ball.x == first.x + 1 && ball.y == first.y + 0 - 1) || (ball.x == second.x + -1 && ball.y == second.y + 0 - 1))))
                {
                    ball.dx *= -1;
                    ball.dy *= -1;
                }
            }

            if (ball.x == 3)
            {
                Console.SetCursorPosition(ball.x, ball.y);
                Console.WriteLine(' ');
                ball.x = 10;
               // ball.dx *= -1;
                ball.y = 10;
                ball.dy *= -1;
                n2++;
            }

            if (ball.x == 77)
            {
                Console.SetCursorPosition(ball.x, ball.y);
                Console.WriteLine(' ');
                ball.x = 70;
               // ball.dx *= -1;
                ball.y = 10;
                ball.dy *= -1;
                n1++;
            }

            Console.SetCursorPosition(ball.x, ball.y);
            Console.WriteLine(' ');
            ball.x += ball.dx;
            ball.y += ball.dy;
            ball.Draw();
        }

            public static void WallUpAndDown()
        {
            for (int i = 2; i < 79; i += 2)
            {
                Console.SetCursorPosition(i, 2);
                Console.WriteLine('☼');
                Console.SetCursorPosition(i, 22);
                Console.WriteLine('☼');
            }
        }
        public static void WallLeftAndRight()
        {
            for (int i = 3; i < 23; i++)
            {
                Console.SetCursorPosition(2, i);
                Console.WriteLine('☼');
                Console.SetCursorPosition(78, i);
                Console.WriteLine('☼');
            }
        }

        public static void Start()
        {
            Console.Beep(440, 200);
            Console.SetCursorPosition(24, 17);
            Console.WriteLine("Нажмите Enter, чтобы начать игру,");
            Console.SetCursorPosition(22, 18);
            Console.WriteLine("или Escape - для выхода из программы.");
            Console.CursorVisible = false;
            while (true)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Enter:
                        Console.SetCursorPosition(24, 19);
                        Console.WriteLine("Введите уровень сложности (1-9).");
                        Console.SetCursorPosition(24, 20);
                        Console.CursorVisible = true;
                        level = Convert.ToInt32(Console.ReadLine());
                        n1 = 0;
                        n2 = 0;
                        Game();
                        break;

                    case ConsoleKey.Escape:
                        Environment.Exit(0);
                        break;

                    default:

                        break;
                }
            }
        }

        public static void Counts(int n1, int n2)
        {
            Console.SetCursorPosition(35, 1);
            Console.WriteLine("Счет - " + n1 + " : " + n2);
        }

        public static void PongWrite()
        {
            Console.SetCursorPosition(27, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(28, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(29, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(30, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(27, 8);
            Console.WriteLine('█');
            Console.SetCursorPosition(31, 8);
            Console.WriteLine('█');
            Console.SetCursorPosition(27, 9);
            Console.WriteLine('█');
            Console.SetCursorPosition(31, 9);
            Console.WriteLine('█');
            Console.SetCursorPosition(27, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(31, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(27, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(28, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(29, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(30, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(27, 12);
            Console.WriteLine('█');
            Console.SetCursorPosition(27, 13);
            Console.WriteLine('█');

            Console.SetCursorPosition(35, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(36, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(37, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(34, 8);
            Console.WriteLine('█');
            Console.SetCursorPosition(38, 8);
            Console.WriteLine('█');
            Console.SetCursorPosition(34, 9);
            Console.WriteLine('█');
            Console.SetCursorPosition(38, 9);
            Console.WriteLine('█');
            Console.SetCursorPosition(34, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(38, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(34, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(38, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(34, 12);
            Console.WriteLine('█');
            Console.SetCursorPosition(38, 12);
            Console.WriteLine('█');
            Console.SetCursorPosition(35, 13);
            Console.WriteLine('█');
            Console.SetCursorPosition(36, 13);
            Console.WriteLine('█');
            Console.SetCursorPosition(37, 13);
            Console.WriteLine('█');
            Console.SetCursorPosition(41, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(45, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(41, 8);
            Console.WriteLine('█');
            Console.SetCursorPosition(45, 8);
            Console.WriteLine('█');
            Console.SetCursorPosition(41, 9);
            Console.WriteLine('█');
            Console.SetCursorPosition(42, 9);
            Console.WriteLine('█');
            Console.SetCursorPosition(45, 9);
            Console.WriteLine('█');
            Console.SetCursorPosition(41, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(43, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(45, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(41, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(44, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(45, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(41, 12);
            Console.WriteLine('█');
            Console.SetCursorPosition(45, 12);
            Console.WriteLine('█');
            Console.SetCursorPosition(41, 13);
            Console.WriteLine('█');
            Console.SetCursorPosition(45, 13);
            Console.WriteLine('█');

            Console.SetCursorPosition(49, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(50, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(51, 7);
            Console.WriteLine('█');
            Console.SetCursorPosition(48, 8);
            Console.WriteLine('█');
            Console.SetCursorPosition(52, 8);
            Console.WriteLine('█');
            Console.SetCursorPosition(48, 9);
            Console.WriteLine('█');
            Console.SetCursorPosition(48, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(50, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(51, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(52, 10);
            Console.WriteLine('█');
            Console.SetCursorPosition(48, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(52, 11);
            Console.WriteLine('█');
            Console.SetCursorPosition(48, 12);
            Console.WriteLine('█');
            Console.SetCursorPosition(52, 12);
            Console.WriteLine('█');
            Console.SetCursorPosition(49, 13);
            Console.WriteLine('█');
            Console.SetCursorPosition(50, 13);
            Console.WriteLine('█');
            Console.SetCursorPosition(51, 13);
            Console.WriteLine('█');
        }
    }
}
